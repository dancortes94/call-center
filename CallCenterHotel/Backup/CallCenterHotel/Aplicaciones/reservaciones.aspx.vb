﻿Imports Npgsql
Imports System.Drawing
Imports iTextSharp
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.IO

Public Class reservaciones
    Inherits System.Web.UI.Page
    'EXPLICACION
    Dim strFechas(100) As String
    Dim strConexion As String = "Server=127.0.0.1;Port=5432;Database=a00105102;uid=a00105102;pwd=roberto"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If IsPostBack Then
        'Else
        '    Try

        '        If User.Identity.IsAuthenticated Then
        '            DropDownList1.Items.Clear()

        '            Dim conn As New NpgsqlConnection(strConexion)
        '            Try
        '                If conn.State = ConnectionState.Closed Then
        '                    conn.Open()
        '                    Dim comando1 As NpgsqlCommand = New NpgsqlCommand("Select distinct(precio) from habitaciones order by precio asc", conn)
        '                    Dim dr1 As NpgsqlDataReader
        '                    dr1 = comando1.ExecuteReader 'dr datareader
        '                    Dim blnExiste As Boolean = False
        '                    Dim strPrecio As String
        '                    While dr1.Read
        '                        strPrecio = dr1.Item("precio")
        '                        DropDownList1.Items.Add(Format(Val(strPrecio), "###,##0.#0"))
        '                    End While
        '                    conn.Close()
        '                End If
        '                TextBox1.Text = Format(Date.Now, "dd/MM/yyyy")
        '                TextBox2.Text = Format(Date.Now, "dd/MM/yyyy")
        '            Catch ex As Exception
        '                Mensaje(ex.ToString)
        '            End Try
        '        Else
        '            Me.Response.Redirect("~/Account/login.aspx")
        '        End If
        '    Catch ex As Exception
        '    End Try
        'End If


        'EXPLICACION
        If IsPostBack Then
        Else
            DropDownList1.Items.Clear()

            Dim conn As New NpgsqlConnection(strConexion)
            Try
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                    Dim comando1 As NpgsqlCommand = New NpgsqlCommand("Select distinct(precio) from habitaciones order by precio asc", conn)
                    Dim dr1 As NpgsqlDataReader
                    dr1 = comando1.ExecuteReader 'dr datareader
                    Dim blnExiste As Boolean = False
                    Dim strPrecio As String
                    While dr1.Read
                        strPrecio = dr1.Item("precio")
                        DropDownList1.Items.Add(Format(Val(strPrecio), "###,##0.#0"))
                    End While
                    conn.Close()
                End If
                If TextBox1.Text = "" Then
                    TextBox1.Text = Format(Date.Now, "dd/MM/yyyy")
                End If
                If TextBox2.Text = "" Then
                    TextBox2.Text = Format(Date.Now.AddDays(6), "dd/MM/yyyy")
                End If
            Catch ex As Exception
                Mensaje(ex.ToString)
            End Try

        End If

    End Sub

    Protected Sub Mensaje(ByVal strMessage As String)
        Dim strScript As String = "<script language=JavaScript>"
        strScript += "alert(""" & strMessage & """);"
        strScript += "</script>"
        Try
            If (Not ClientScript.IsStartupScriptRegistered("clientScript")) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "clientScript", strScript)
            End If
        Catch
        End Try
    End Sub

    Protected Sub btnReservacion_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnReservacion.Click
        'BOTON OBSOLETO
        Dim strTarifa As String = QuitaComas(DropDownList1.Text)
        Dim dtmFechaInicial As Date = CDate(TextBox1.Text)
        Dim dtmFechaFinal As Date = CDate(TextBox2.Text)
        Dim strFecha As String
        Dim strSQL As String
        Dim strHabitacion As String
        Dim strHora As String
        Dim contador As Integer = 0
        Dim conn As New NpgsqlConnection(strConexion)
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
                Dim comando1 As NpgsqlCommand = New NpgsqlCommand("Select * from habitaciones where precio = " & Val(strTarifa), conn)

                'lo nuevo
                Dim dt1 As New DataTable
                Dim dt2 As New DataTable
                dt1.TableName = "habitaciones"
                dt2.TableName = "reservaciones"
                dt1.Load(comando1.ExecuteReader)
                For Each Fila1 As DataRow In dt1.Rows
                    strHabitacion = Fila1.Item(0)
                    dtmFechaInicial = CDate(TextBox1.Text)
                    While dtmFechaInicial <= dtmFechaFinal And contador < Val(TextBox4.Text)
                        strFecha = Format(dtmFechaInicial, "yyyy/MM/dd")
                        strSQL = "Select * from reservacionesaspx where habitacion = '" & strHabitacion & "' and fecha ='" & strFecha & "'"
                        Dim comando2 As NpgsqlCommand = New NpgsqlCommand(strSQL, conn)
                        dt2.Load(comando2.ExecuteReader)
                        If dt2.Rows.Count = 0 Then
                            'habitacion libre
                            strHora = Format(Now.Hour, "HH") + Format(Now.Minute, "MM") + Format(Now.Second, "SS")
                            strSQL = "insert into reservacionesaspx (habitacion,precio,fecha,anombrede) values ("
                            strSQL = strSQL + "'" & strHabitacion & "',"
                            strSQL = strSQL & Val(strTarifa) & ","
                            strSQL = strSQL + "'" & strFecha & "',"
                            strSQL = strSQL + "'" & TextBox3.Text & "')"
                            'strSQL = strSQL + "'" & strHora & "')"
                            Dim comando3 As NpgsqlCommand = New NpgsqlCommand(strSQL, conn)
                            comando3.ExecuteNonQuery()
                            contador = contador + 1

                        End If
                        dtmFechaInicial = dtmFechaInicial.AddDays(1)
                    End While

                Next
                If contador = Val(TextBox4.Text) Then
                    Mensaje("Reservacion exitosa")
                Else
                    Mensaje("Reservacion parcial" + contador.ToString)
                End If
                conn.Close()
            End If
            TextBox1.Text = Format(Date.Now, "dd/MM/yyyy")
            TextBox2.Text = Format(Date.Now, "dd/MM/yyyy")
        Catch ex As Exception
            Mensaje(ex.ToString)
        End Try

    End Sub
    Private Function QuitaComas(ByVal strQ As String) As String

        Dim intI As Integer
        Dim strI As String = ""
        For intI = 1 To Len(strQ)
            If (Mid(strQ, intI, 1) >= "0" And Mid(strQ, intI, 1) <= "9") Or Mid(strQ, intI, 1) = "." Then
                strI = strI + Mid(strQ, intI, 1)
            End If
        Next
        Return strI
    End Function
    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs) Handles Button1.Click
        Dim headerRow As GridViewRow = GridView2.HeaderRow
        Dim dt1 As New DataTable
        Dim dt2 As New DataTable
        dt1.TableName = "habitaciones"
        dt2.TableName = "reservaciones"
        Dim strHabitacion As String
        Dim strFecha As String = ""
        Dim intI As Integer = 0
        Dim strANombreDe As String = TextBox3.Text
        Dim strTarifa As String = ""
        Dim strSQL As String = ""
        Dim strHora As String = ""
        If strANombreDe = "" Then
            Mensaje("Falta a nombre de quien la reservacion")
        Else
            Dim conn As New NpgsqlConnection(strConexion)
            Try
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                    For Each row2 As GridViewRow In GridView2.Rows
                        If row2.RowType = DataControlRowType.DataRow Then
                            'EXPLICACION
                            Dim chkRow0 As CheckBox = TryCast(row2.Cells(0).FindControl("chkRow1"), CheckBox)
                            If chkRow0.Checked Then
                                strHabitacion = row2.Cells(1).Text
                                strTarifa = row2.Cells(3).Text
                                For intI = 4 To 10
                                    If row2.Cells(intI).Text = "O" Then
                                        'EXPLICACION
                                        strFecha = headerRow.Cells(intI).Text

                                        'strFecha = strFechas(intI - 3)
                                        strSQL = "Select * from reservacionesaspx where habitacion = '" & strHabitacion & "' and fecha ='" & strFecha & "'"
                                        Dim comando2 As NpgsqlCommand = New NpgsqlCommand(strSQL, conn)
                                        dt2.Load(comando2.ExecuteReader)
                                        If dt2.Rows.Count = 0 Then
                                            'habitacion libre
                                            strHora = Format(Now.Hour, "HH") + Format(Now.Minute, "MM") + Format(Now.Second, "SS")
                                            strSQL = "insert into reservacionesaspx (habitacion,precio,fecha,anombrede) values ("
                                            strSQL = strSQL + "'" & strHabitacion & "',"
                                            strSQL = strSQL & Val(strTarifa) & ","
                                            strSQL = strSQL + "'" & strFecha & "',"
                                            strSQL = strSQL + "'" & TextBox3.Text & "')"
                                            'strSQL = strSQL + "'" & strHora & "')"
                                            Dim comando3 As NpgsqlCommand = New NpgsqlCommand(strSQL, conn)
                                            comando3.ExecuteNonQuery()
                                        End If
                                    End If
                                Next
                            End If
                        End If
                    Next
                    conn.Close()
                End If
            Catch ex As Exception
                Mensaje(ex.ToString)
            End Try
        End If
    End Sub
    Protected Sub btnPrepara_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPrepara.Click
        'paso no.1 preparo encabezados al gridview2
        Dim tabla1 As New DataTable
        'encabezados
        Dim strXO(100) As String
        'abrir base de datos con tabla habitaciones
        Dim strTarifa As String = QuitaComas(DropDownList1.Text)
        Dim dtmFechaInicial As Date = CDate(TextBox1.Text)
        Dim dtmFechaFinal As Date = CDate(TextBox2.Text)
        Dim strSQL As String
        Dim strHabitacion As String
        Dim strHora As String = ""
        Dim intI As Integer = 1
        Dim intD As Integer = 0
        Dim strDescripcion As String = ""
        Dim contador As Integer = 0
        Dim conn As New NpgsqlConnection(strConexion)
        'EXPLICACION
        For intI = 1 To 100
            strFechas(intI) = Format(dtmFechaInicial, "yyyy/MM/dd")
            dtmFechaInicial = dtmFechaInicial.AddDays(1)
        Next
        dtmFechaInicial = CDate(TextBox1.Text)
        dtmFechaFinal = CDate(TextBox2.Text)
        intI = 1
        While dtmFechaInicial <= dtmFechaFinal
            strFechas(intI) = Format(dtmFechaInicial, "yyyy/MM/dd")
            dtmFechaInicial = dtmFechaInicial.AddDays(1)
            intD = intD + 1
            intI = intI + 1
        End While
        tabla1.Columns.AddRange(New DataColumn(9) {New DataColumn("Habitación"), New DataColumn("Descripción"), New DataColumn("Precio"), New DataColumn(strFechas(1)), New DataColumn(strFechas(2)), New DataColumn(strFechas(3)), New DataColumn(strFechas(4)), New DataColumn(strFechas(5)), New DataColumn(strFechas(6)), New DataColumn(strFechas(7))})
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
                Dim comando1 As NpgsqlCommand = New NpgsqlCommand("Select * from habitaciones order by precio", conn)

                'lo nuevo
                Dim dt1 As New DataTable
                Dim dt2 As New DataTable
                Dim strPrecio
                dt1.TableName = "habitaciones"
                dt2.TableName = "reservaciones"
                dt1.Load(comando1.ExecuteReader)
                For Each Fila1 As DataRow In dt1.Rows
                    strHabitacion = Fila1.Item(0)
                    strDescripcion = Fila1.Item(1)
                    strPrecio = Fila1.Item(2)
                    'limpio buffer de X y O s
                    'EXPLICACION
                    For intI = 1 To 7
                        strXO(intI) = "-"
                    Next
                    For intI = 1 To intD
                        strSQL = "Select * from reservacionesaspx where habitacion = '" & strHabitacion & "' and fecha ='" & strFechas(intI) & "'"
                        Dim comando2 As NpgsqlCommand = New NpgsqlCommand(strSQL, conn)
                        dt2.Load(comando2.ExecuteReader)
                        'EXPLICACION
                        If dt2.Rows.Count = 0 Then
                            'habitacion libre
                            strXO(intI) = "O"
                        Else
                            'habitacion ocupada
                            strXO(intI) = "X"
                        End If
                        dt2.Clear()
                    Next
                    'EXPLICACION
                    tabla1.Rows.Add(strHabitacion, strDescripcion, strPrecio, strXO(1), strXO(2), strXO(3), strXO(4), strXO(5), strXO(6), strXO(7))
                Next
                GridView2.DataSource = tabla1
                GridView2.DataBind()

                Mensaje("Revise sus datos")
                conn.Close()
            End If
            'TextBox1.Text = Format(Date.Now, "dd/MM/yyyy")
            'TextBox2.Text = Format(Date.Now, "dd/MM/yyyy")
        Catch ex As Exception
            Mensaje(ex.ToString)
        End Try


    End Sub

    Private Sub GridView2_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles GridView2.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            'EXPLICACION
            Dim intI As Integer
            For intI = 4 To 10
                If e.Row.Cells(intI).Text = "O" Then
                    e.Row.Cells(intI).BackColor = Color.LightGreen
                Else
                    If e.Row.Cells(intI).Text = "X" Then
                        e.Row.Cells(intI).BackColor = Color.Red
                    Else
                        e.Row.Cells(intI).BackColor = Color.Yellow
                    End If

                End If

            Next
        End If
    End Sub
    Protected Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim headerRow As GridViewRow = GridView2.HeaderRow
        Dim dt1 As New DataTable
        Dim dt2 As New DataTable
        dt1.TableName = "habitaciones"
        dt2.TableName = "reservaciones"
        Dim strHabitacion As String
        Dim strFecha As String = ""
        Dim intI As Integer = 0
        Dim strANombreDe As String = TextBox3.Text
        Dim strTarifa As String = ""
        Dim strSQL As String = ""
        Dim strHora As String = ""
        If strANombreDe = "" Then
            Mensaje("Falta a nombre de quien la reservacion")
        Else
            Dim conn As New NpgsqlConnection(strConexion)
            Try
                If conn.State = ConnectionState.Closed Then
                    conn.Open()
                    For Each row2 As GridViewRow In GridView2.Rows
                        If row2.RowType = DataControlRowType.DataRow Then
                            'EXPLICACION
                            Dim chkRow0 As CheckBox = TryCast(row2.Cells(0).FindControl("chkRow1"), CheckBox)
                            If chkRow0.Checked Then
                                strHabitacion = row2.Cells(1).Text
                                strTarifa = row2.Cells(3).Text
                                For intI = 4 To 10
                                    If row2.Cells(intI).Text = "X" Then
                                        'EXPLICACION
                                        strFecha = headerRow.Cells(intI).Text
                                        'strFecha = strFechas(intI - 3)
                                        strSQL = "Select * from reservacionesaspx where habitacion = '" & strHabitacion & "' and fecha ='" & strFecha & "'"
                                        Dim comando2 As NpgsqlCommand = New NpgsqlCommand(strSQL, conn)
                                        dt2.Load(comando2.ExecuteReader)
                                        If dt2.Rows.Count = 0 Then
                                            'habitacion libre
                                        Else
                                            strSQL = "delete from reservacionesaspx where fecha='" & strFecha & "' and habitacion='" & strHabitacion & "'"
                                            Dim comando3 As NpgsqlCommand = New NpgsqlCommand(strSQL, conn)
                                            comando3.ExecuteNonQuery()
                                        End If
                                    End If
                                Next
                            End If
                        End If
                    Next
                    conn.Close()
                End If
            Catch ex As Exception
                Mensaje(ex.ToString)
            End Try
        End If
    End Sub

    Protected Sub BtnChkOutShow_Click(sender As Object, e As EventArgs) Handles BtnChkOutShow.Click
        Response.Redirect("~/aplicaciones/checkout.aspx")
    End Sub
End Class