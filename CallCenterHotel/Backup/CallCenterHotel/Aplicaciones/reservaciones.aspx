﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="reservaciones.aspx.vb" Inherits="CallCenterHotel.reservaciones" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            width: 911px;
        }
        .style2
        {
            width: 849px;
        }
        .style3
        {
            width: 817px;
        }
        .style4
        {
            width: 728px;
        }
        .style5
        {
            width: 848px;
        }
        .style6
        {
            width: 848px;
            height: 31px;
        }
        .style7
        {
            width: 728px;
            height: 31px;
        }
        .style8
        {
            width: 817px;
            height: 31px;
        }
        .style9
        {
            width: 849px;
            height: 31px;
        }
        .style10
        {
            width: 911px;
            height: 31px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table style="margin-right: 0px; width: 917px;">
    <tr>
        <td class="style5">Programa de reservaciones</td>
        <td class="style4">
            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
        </td>
        <td class="style3"></td>
        <td class="style2">&nbsp;</td>
        <td class="style1"></td>
    </tr>
    <tr>
        <td class="style6">Fecha inicial</td>
        <td class="style7">
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        </td>
        <td class="style8"></td>
        <td class="style9">&nbsp;</td>
        <td class="style10"></td>
    </tr>
    <tr>
        <td class="style5">Fecha de salida</td>
        <td class="style4">
            <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
        </td>
        <td class="style3"></td>
        <td class="style2">&nbsp;</td>
        <td class="style1">
        </td>
    </tr>
    <tr>
        <td class="style5">Tarifa</td>
        <td class="style4">
            <asp:DropDownList ID="DropDownList1" runat="server">
            </asp:DropDownList>
        </td>
        <td class="style3">
            <asp:Button ID="btnPrepara" runat="server" Text="Prepara" Height="24px" />
            </td>
        <td class="style2">&nbsp;</td>
        <td class="style1">
            &nbsp;</td>
    </tr>
    <tr>
        <td class="style5">A nombre de </td>
        <td class="style4">
            <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
        </td>
        <td class="style3">
            <asp:Button ID="Button1" runat="server" Text="Actualiza" />
            </td>
        <td class="style2">&nbsp;</td>
        <td class="style1">&nbsp;</td>
    </tr>
    <tr>
        <td class="style5">Numero de habitaciones</td>
        <td class="style4">
            <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
        </td>
        <td class="style3">
            <asp:Button ID="Button3" runat="server" Text="Borrar" Width="83px" />
        </td>
        <td class="style2">&nbsp;</td>
        <td class="style1">&nbsp;</td>
    </tr>
    <tr>
        <td class="style5">
            <asp:Button ID="btnReservacion" runat="server" Text="Reservación" />
        </td>
        <td class="style4">
            &nbsp;</td>
        <td class="style3">
            <asp:Button ID="BtnChkOutShow" runat="server" Text="Check Out" />
        </td>
        <td class="style2">
            &nbsp;</td>
        <td class="style1">&nbsp;</td>
    </tr>
    <tr>
        <td class="style5">
            &nbsp;</td>
        <td class="style4">
            <asp:GridView ID="GridView2" runat="server" Height="86px" Width="395px">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkRow1" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </td>
        <td class="style3">
            &nbsp;</td>
        <td class="style2">&nbsp;</td>
        <td class="style1">&nbsp;</td>
    </tr>
</table>
</asp:Content>
