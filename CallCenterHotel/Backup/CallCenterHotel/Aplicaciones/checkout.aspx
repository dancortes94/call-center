﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="checkout.aspx.vb" Inherits="CallCenterHotel.checkout" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            width: 189px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table style="margin-right: 0px; width: 917px;">
        <tr>
            <td class="style1">Checkout</td>
            <td class="style1">&nbsp;</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td class="style1">&nbsp;</td>
            <td class="style1">Número de habitación</td>
            <td>
                <asp:TextBox ID="txtNumHabitacion" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style1">&nbsp;</td>
            <td class="style1">RFC</td>
            <td>
                <asp:TextBox ID="txtRFC" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style1">&nbsp;</td>
            <td class="style1">Fecha de Salida</td>
            <td>
                <asp:TextBox ID="txtFechaOut" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style1">&nbsp;</td>
            <td class="style1">&nbsp;</td>
            <td>
                <asp:Button ID="BtnCheckOut" runat="server" Text="Check Out" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style1">&nbsp;</td>
            <td class="style1">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>
