﻿Imports Npgsql
Imports System.Drawing
Imports iTextSharp
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.IO
Public Class checkout
    Inherits System.Web.UI.Page
    Dim strFechas(100) As String
    Dim strConexion As String = "Server=127.0.0.1;Port=5432;Database=a00105102;uid=a00105102;pwd=roberto"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If txtFechaIn.Text = "" Then
            txtFechaIn.Text = Format(Date.Now, "dd/MM/yyyy")
        End If
        If txtFechaOut.Text = "" Then
            txtFechaOut.Text = Format(Date.Now.AddDays(6), "dd/MM/yyyy")
        End If
    End Sub
    Protected Sub Mensaje(ByVal strMessage As String)
        Dim strScript As String = "<script language=JavaScript>"
        strScript += "alert(""" & strMessage & """);"
        strScript += "</script>"
        Try
            If (Not ClientScript.IsStartupScriptRegistered("clientScript")) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "clientScript", strScript)
            End If
        Catch
        End Try
    End Sub

    Protected Sub BtnCheckOut_Click(sender As Object, e As EventArgs) Handles BtnCheckOut.Click
        Dim strFechaIngreso As String = CDate(txtFechaIn.Text)
        Dim strFechaSalida As String = CDate(txtFechaOut.Text)
        Dim strHabitacion As String = txtNumHabitacion.Text
        Dim strRFC As String = txtRFC.Text
        Dim strNombre As String = ""
        Dim intPrecioUnit As Integer = 0
        Dim intTotal As Integer = 0
        Dim conn As New NpgsqlConnection(strConexion)
        Dim intDias As Integer = Microsoft.VisualBasic.DateAndTime.Day(strFechaSalida) - Microsoft.VisualBasic.DateAndTime.Day(strFechaIngreso)
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
                Dim strSQL As String = "select * from reservacionesaspx where fechain='" & strFechaIngreso & "' and rfc='" & strRFC & "'"
                Dim comando1 As NpgsqlCommand = New NpgsqlCommand(strSQL, conn)
                Dim dt1 As New DataTable
                dt1.TableName = "reservaciones"
                dt1.Load(comando1.ExecuteReader)
                For Each Fila1 As DataRow In dt1.Rows
                    strNombre = Fila1.Item(3)
                    intPrecioUnit = Fila1.Item(1)
                Next
                intTotal = intDias * intPrecioUnit
                strSQL = "update reservacionesaspx set fechaout='" & strFechaSalida & "' "
                strSQL = strSQL + "where habitacion='" & strHabitacion & "' and rfc = '" & strRFC & "' and fechain='" & strFechaIngreso & "';"
                Dim comando2 As NpgsqlCommand = New NpgsqlCommand(strSQL, conn)
                comando2.ExecuteNonQuery()
                Mensaje("El precio es de "+intTotal.ToString)
                Call PDFII(strHabitacion, strNombre, strRFC, strFechaIngreso, strFechaSalida, intDias, intPrecioUnit, intTotal)
            End If
            conn.Close()
        Catch ex As Exception

        End Try
    End Sub
    Public Sub PDFII(ByVal strH As String, ByVal strN As String, ByVal strR As String, ByVal strFI As String, ByVal strFO As String, ByVal iDDE As Integer, ByVal intP As Integer, ByVal intPT As Integer)
        Dim strTitulo1 As String = "Checkout"
        Dim strTitulo2 As String = "Hotel Anáhuac"
        Dim Xo As Double = 0
        Dim Yo As Double = 0
        Dim ruta As String = "C:\Users\Daniel_2\Documents\Universidad\7mo Semestre\Nuevos Paradigmas\pdfs\x22.pdf"
        Dim documento As New iTextSharp.text.Document(PageSize.LETTER, 72, 72, 72, 72)
        Dim pdfw As iTextSharp.text.pdf.PdfWriter
        Dim fuente As iTextSharp.text.pdf.BaseFont
        Dim fuente1 As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte
        pdfw = PdfWriter.GetInstance(documento, New FileStream(ruta, FileMode.Create, FileAccess.Write, FileShare.None))
        Dim intTam As Double = documento.PageSize.Height - 15
        fuente = FontFactory.GetFont(FontFactory.COURIER, iTextSharp.text.Font.DEFAULTSIZE, iTextSharp.text.Font.NORMAL).BaseFont
        fuente1 = FontFactory.GetFont(FontFactory.COURIER, iTextSharp.text.Font.DEFAULTSIZE, iTextSharp.text.Font.BOLD).BaseFont
        Try
            'Apertura del documento.
            documento.Open()
            'Agregamos una pagina.
            documento.NewPage()
            Dim intCont As Integer = 0
            cb = pdfw.DirectContent
            cb.BeginText()
            cb.SetFontAndSize(fuente1, 8)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, strTitulo1, 100, intTam - 8, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, strTitulo2, 100, intTam - 18, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Habitacion", 10, intTam - 28, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Nombre", 100, intTam - 28, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "RFC", 150, intTam - 28, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Fecha de Ingreso", 200, intTam - 28, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Fecha de Salida", 300, intTam - 28, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Dias", 400, intTam - 28, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Precio Unit", 450, intTam - 28, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Precio Total", 550, intTam - 28, 0)

            cb.SetFontAndSize(fuente, 8)
            Yo = 38

            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, strH, 10, intTam - Yo, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, strN, 100, intTam - Yo, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, strR, 150, intTam - Yo, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, strFI, 200, intTam - Yo, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, strFO, 300, intTam - Yo, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, iDDE, 400, intTam - Yo, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, intP, 450, intTam - Yo, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, intPT, 550, intTam - Yo, 0)
            cb.EndText()

            'Forzamos vaciamiento del buffer.
            pdfw.Flush()
            'Cerramos el documento.
            documento.Close()

            pdfw = Nothing
            documento = Nothing


            System.Diagnostics.Process.Start("C:\Users\Daniel_2\Documents\Universidad\7mo Semestre\Nuevos Paradigmas\pdfs\x22.pdf")
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Response.Redirect("~/aplicaciones/reservaciones.aspx")
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Response.Redirect("~/aplicaciones/checkin.aspx")
    End Sub
End Class