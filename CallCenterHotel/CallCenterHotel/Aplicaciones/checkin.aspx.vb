﻿Imports Npgsql
Imports System.Drawing
Imports iTextSharp
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.IO
Public Class checkin
    Inherits System.Web.UI.Page
    Dim strFechas(100) As String
    Dim strHabitacion As String
    Dim strConexion As String = "Server=127.0.0.1;Port=5432;Database=a00105102;uid=a00105102;pwd=roberto"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If txtFechaIn.Text = "" Then
            txtFechaIn.Text = Format(Date.Now, "dd/MM/yyyy")
        End If
        If txtFechaOut.Text = "" Then
            txtFechaOut.Text = Format(Date.Now.AddDays(6), "dd/MM/yyyy")
        End If
    End Sub
    Protected Sub BtnBuscar_Click(sender As Object, e As EventArgs) Handles BtnBuscar.Click
        'paso no.1 preparo encabezados al gridview2
        Dim tabla1 As New DataTable
        'abrir base de datos con tabla habitaciones
        Dim dtmFechaInicial As Date = CDate(txtFechaIn.Text)
        Dim dtmFechaFinal As Date = CDate(txtFechaOut.Text)
        Dim strFechas As String
        Dim strHora As String = ""
        Dim strSQL As String = ""
        Dim intI As Integer = 1
        Dim intD As Integer = 0
        Dim strDia As String = ""
        Dim contador As Integer = 0
        Dim conn As New NpgsqlConnection(strConexion)

        strFechas = ""
        While dtmFechaInicial <= dtmFechaFinal
            If dtmFechaInicial = dtmFechaFinal Then
                strFechas = strFechas + "'" & Format(dtmFechaInicial, "yyyy/MM/dd") & "'"
            Else
                strFechas = strFechas + "'" & Format(dtmFechaInicial, "yyyy/MM/dd") & "', "
            End If
            dtmFechaInicial = dtmFechaInicial.AddDays(1)
        End While

        tabla1.Columns.AddRange(New DataColumn(1) {New DataColumn("Habitación"), New DataColumn("Día")})
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()

                strSQL = "SELECT * FROM reservacionesaspx WHERE anombrede = '" & txtNombre.Text & "' "
                strSQL = strSQL + "AND fecha IN (" & strFechas & ")"

                Dim comando1 As NpgsqlCommand = New NpgsqlCommand(strSQL, conn)

                'lo nuevo
                Dim dt1 As New DataTable
                dt1.TableName = "reservaciones"
                dt1.Load(comando1.ExecuteReader)
                'Mensaje(dt1.Rows.Count().ToString)
                For Each Fila1 As DataRow In dt1.Rows
                    strHabitacion = Fila1.Item(0)
                    strDia = Fila1.Item(2)
                    tabla1.Rows.Add(strHabitacion, strDia)
                    'PREGUNTAR
                    GridView2.DataSource = tabla1
                    GridView2.DataBind()
                Next

                'GridView2.DataSource = tabla1
                'GridView2.DataBind()

                Mensaje("Revise sus datos")
                conn.Close()
            End If
        Catch ex As Exception
            Mensaje(ex.ToString)
        End Try
    End Sub
    Protected Sub BtnCheckIn_Click(sender As Object, e As EventArgs) Handles BtnCheckIn.Click
        Dim strFechaIn As String = CDate(txtFechaIn.Text)
        Dim strFechaOut As String = CDate(txtFechaOut.Text)
        Dim strNombre As String = txtNombre.Text
        Dim strRFC As String = txtRFC.Text
        Dim conn As New NpgsqlConnection(strConexion)
        Try
            If conn.State = ConnectionState.Closed Then
                conn.Open()
                For Each row2 As GridViewRow In GridView2.Rows
                    If row2.RowType = DataControlRowType.DataRow Then
                        'EXPLICACION
                        Dim chkRow0 As CheckBox = TryCast(row2.Cells(0).FindControl("chkRow1"), CheckBox)
                        If chkRow0.Checked Then
                            strHabitacion = row2.Cells(1).Text
                        End If
                    End If
                Next
                Dim strSQL As String = "update reservacionesaspx set rfc='" & strRFC & "', fechaout='" & strFechaOut & "', fechain= '" & strFechaIn & "'"
                strSQL = strSQL + " where habitacion='" & strHabitacion & "' and anombrede= '" & strNombre & "';"
                Dim comando1 As NpgsqlCommand = New NpgsqlCommand(strSQL, conn)
                comando1.ExecuteNonQuery()
                Call PDFII(strHabitacion, strNombre, strRFC, strFechaIn, strFechaOut)
                Mensaje("Exito")
                GridView2.Visible = False
                conn.Close()
            End If
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub Mensaje(ByVal strMessage As String)
        Dim strScript As String = "<script language=JavaScript>"
        strScript += "alert(""" & strMessage & """);"
        strScript += "</script>"
        Try
            If (Not ClientScript.IsStartupScriptRegistered("clientScript")) Then
                ClientScript.RegisterClientScriptBlock(Me.GetType(), "clientScript", strScript)
            End If
        Catch
        End Try
    End Sub
    Public Sub PDFII(ByVal strH As String, ByVal strN As String, ByVal strR As String, ByVal strFI As String, ByVal strFO As String)
        Dim strTitulo1 As String = "Check In"
        Dim strTitulo2 As String = "Hotel Anáhuac"
        Dim Xo As Double = 0
        Dim Yo As Double = 0
        Dim ruta As String = "C:\Users\Daniel_2\Documents\Universidad\7mo Semestre\Nuevos Paradigmas\pdfs\x22.pdf"
        Dim documento As New iTextSharp.text.Document(PageSize.LETTER, 72, 72, 72, 72)
        Dim pdfw As iTextSharp.text.pdf.PdfWriter
        Dim fuente As iTextSharp.text.pdf.BaseFont
        Dim fuente1 As iTextSharp.text.pdf.BaseFont
        Dim cb As PdfContentByte
        pdfw = PdfWriter.GetInstance(documento, New FileStream(ruta, FileMode.Create, FileAccess.Write, FileShare.None))
        Dim intTam As Double = documento.PageSize.Height - 15
        fuente = FontFactory.GetFont(FontFactory.COURIER, iTextSharp.text.Font.DEFAULTSIZE, iTextSharp.text.Font.NORMAL).BaseFont
        fuente1 = FontFactory.GetFont(FontFactory.COURIER, iTextSharp.text.Font.DEFAULTSIZE, iTextSharp.text.Font.BOLD).BaseFont
        Try
            'Apertura del documento.
            documento.Open()
            'Agregamos una pagina.
            documento.NewPage()
            Dim intCont As Integer = 0
            cb = pdfw.DirectContent
            cb.BeginText()
            cb.SetFontAndSize(fuente1, 8)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, strTitulo1, 100, intTam - 8, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, strTitulo2, 100, intTam - 18, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Habitacion", 10, intTam - 28, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Nombre", 100, intTam - 28, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "RFC", 150, intTam - 28, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Fecha de Ingreso", 200, intTam - 28, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, "Fecha de Salida", 300, intTam - 28, 0)

            cb.SetFontAndSize(fuente, 8)
            Yo = 38

            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, strH, 10, intTam - Yo, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, strN, 100, intTam - Yo, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, strR, 150, intTam - Yo, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, strFI, 200, intTam - Yo, 0)
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, strFO, 300, intTam - Yo, 0)
            cb.EndText()

            'Forzamos vaciamiento del buffer.
            pdfw.Flush()
            'Cerramos el documento.
            documento.Close()

            pdfw = Nothing
            documento = Nothing


            System.Diagnostics.Process.Start("C:\Users\Daniel_2\Documents\Universidad\7mo Semestre\Nuevos Paradigmas\pdfs\x22.pdf")
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Protected Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Response.Redirect("~/aplicaciones/checkout.aspx")
    End Sub

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Response.Redirect("~/aplicaciones/reservaciones.aspx")
    End Sub
End Class