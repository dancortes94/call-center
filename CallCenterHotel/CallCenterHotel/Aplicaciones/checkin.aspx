﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="checkin.aspx.vb" Inherits="CallCenterHotel.checkin" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .style1
        {
            width: 189px;
        }
        .auto-style1 {
            width: 189px;
            height: 82px;
        }
        .auto-style2 {
            height: 82px;
        }
        .auto-style3 {
            width: 917px;
            height: 204px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table style="margin-right: 0px; " class="auto-style3">
        <tr>
            <td class="style1">CheckIn</td>
            <td class="style1">&nbsp;</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td class="style1">&nbsp;</td>
            <td class="style1">Nombre</td>
            <td>
                <asp:TextBox ID="txtNombre" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style1">&nbsp;</td>
            <td class="style1">RFC</td>
            <td>
                <asp:TextBox ID="txtRFC" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style1">&nbsp;</td>
            <td class="style1">Fecha de Entrada</td>
            <td>
                <asp:TextBox ID="txtFechaIn" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style1">&nbsp;</td>
            <td class="style1">Fecha de Salida</td>
            <td>
                <asp:TextBox ID="txtFechaOut" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td class="style1">&nbsp;</td>
            <td class="style1">
                <asp:Button ID="BtnBuscar" runat="server" Text="Buscar Reservacion" />
            </td>
            <td>
                &nbsp;</td>
            <td></td>
        </tr>
        <tr>
            <td class="style1">&nbsp;</td>
            <td class="style1">
                <asp:Button ID="BtnCheckIn" runat="server" Text="Check In" />
            </td>
            <td>
                &nbsp;</td>
            <td></td>
        </tr>
        <tr>
            <td class="style5">
            &nbsp;</td>
            <td class="style5">
                <asp:Button ID="Button1" runat="server" Text="Reservaciones" />
                <br />
                <br />
                <asp:Button ID="Button2" runat="server" Text="Check Out" />
            </td>
        <td class="style4">
            <asp:GridView ID="GridView2" runat="server" Height="86px" Width="395px">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkRow1" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </td>
        </tr>
    </table>
</asp:Content>
